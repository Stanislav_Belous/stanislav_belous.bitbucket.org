var callbackRegistry = {};
var resultRequest;

(function () {

    var searchButton, inputSearch, request, body,
        arrayWithVideos = null, swipeSection, sectionLeft, sectionRight;
    var numbVideoOnDisplay, currentPage, currentSection, currentPageActive;
    var mouseClick = false, successRequest, endSwipe = true;
    var oldY, oldX, delta;
    var leftSwipe, rightSwipe;

    (function loadHTML() {
        var localeValue = window.localStorage.getItem('request'),
            request = false;
        var mainDiv = document.createElement('div'),
            div = document.createElement('div'),
            divForm = document.createElement('div'),
            input = document.createElement('input'),
            h1Search = document.createElement('h1'),
            h1Tube = document.createElement('h1'),
            buttonSearch = document.createElement('button');

        h1Search.innerHTML = 'Search';
        h1Tube.innerHTML = 'Tube';
        buttonSearch.innerHTML = 'Search';

        input.id = 'search_text';
        buttonSearch.id = 'search_button';


        input.setAttribute('autofocus', 'true');
        input.setAttribute('type', 'search');
        input.setAttribute('placeholder', 'Example: JavaScript');

        h1Search.className = 'inscription-search';
        h1Tube.className = 'inscription-tube';
        buttonSearch.className = 'button-search';
        input.className = 'text-search';

        if(  localeValue === null ) {
            div.className = 'logo';
            divForm.className = 'search';
            mainDiv.className = 'body-form';
        }
        else {
            div.className = 'logo-top';
            divForm.className = 'search-top';
            input.value = localeValue;
            mainDiv.className = 'body-form-top';
            request = true;
        };

        div.appendChild(h1Search);
        div.appendChild(h1Tube);


        divForm.appendChild(input);
        divForm.appendChild(buttonSearch);

        mainDiv.appendChild(div);
        mainDiv.appendChild(divForm);

        document.body.appendChild(mainDiv);

        initVariables();

        if( request)
            sendRequest();
    })();

    function initVariables() {
        searchButton = document.querySelector('#search_button');
        inputSearch = document.querySelector('#search_text');
        body = document.body;
        var screenWidth = window.innerWidth;
        if( screenWidth > 945 )
            numbVideoOnDisplay = 3;
        else if ( screenWidth <= 945 && screenWidth > 600 )
            numbVideoOnDisplay = 2;
        else
            numbVideoOnDisplay = 1;

        searchButton.onclick = function () {
            sendRequest();
        };
    }

    function createItemVideo( data, id ) {

        itemVideo = document.createElement('div');
        imgLink =  document.createElement('a');
        img = document.createElement('img');
        divLikesViews = document.createElement('div');
        h3LV =  document.createElement('h3');
        h1TittleLink =  document.createElement('a');
        divAbout =  document.createElement('div');

        itemVideo.className = 'video-item';
        img.className = 'video-image';
        h1TittleLink.className = 'video-tittle';
        divAbout.className = 'video-info';
        itemVideo.setAttribute('id', 'video_item__'+id);
        h1TittleLink.setAttribute('href', data.link[0].href );
        imgLink.setAttribute('href', data.link[0].href );

        img.setAttribute( 'src', data.media$group.media$thumbnail[1].url );
        imgLink.appendChild(img);
        itemVideo.appendChild(imgLink);

        divLikesViews.className = 'likes-and-views';
        h3LV.appendChild( document.createTextNode('Views:'));
        divLikesViews.appendChild(h3LV);
        h3LV =  document.createElement('h3');

        try {
            h3LV.appendChild(document.createTextNode(data.yt$statistics.viewCount));
        }
        catch ( err ){
            h3LV.appendChild( document.createTextNode( '0' ) );
        }
        divLikesViews.appendChild(h3LV);
        h3LV =  document.createElement('h3');

        itemVideo.appendChild(divLikesViews);

        divLikesViews =  document.createElement('div');
        divLikesViews.className = 'likes-and-views';

        h3LV.appendChild( document.createTextNode( 'Author:' ) );
        divLikesViews.appendChild(h3LV);
        h3LV =  document.createElement('h3');

        h3LV.appendChild( document.createTextNode( data.author[0].name.$t ) );
        divLikesViews.appendChild(h3LV);
        h1TittleLink.appendChild( document.createTextNode( data.title.$t ) );
        divAbout.appendChild( document.createTextNode( data.media$group.media$description.$t ) );

        itemVideo.appendChild(divLikesViews);
        itemVideo.appendChild(h1TittleLink);
        itemVideo.appendChild(divAbout);

        return itemVideo;
    }

    function sendRequest() {
        var callbackName = 'f'+String(Math.random()).slice(2);
        successRequest = false;
        var src, script;
        var inputSearch = document.querySelector('#search_text');

        if (inputSearch.value.trim() === '') {
            request = null;
        }
        else {

            if( body.querySelector('.search') !== null ) {
                body.querySelector('.logo').className = 'logo-top';
                body.querySelector('.search').className = 'search-top';
                body.querySelector('.body-form').className = 'body-form-top';
            }
            request = inputSearch.value.trim();

            src = 'http://gdata.youtube.com/feeds/api/videos/?' +
                'callback=callbackRegistry.' + callbackName +
                '&v=2&alt=json&' +
                'max-results=15&start-index=' + 1 +
                '&q='+ request;

            script = document.createElement('script');
            script.src = src;
            script.setAttribute('data-request', callbackName);
            body.appendChild(script);

            inputSearch.value = request;
        }

        callbackRegistry[callbackName] = function(data) {


            resultRequest = data.feed.entry;
            arrayWithVideos = resultRequest;

            var itemVideo, img, divLikesViews, h3LV, h1Tittle, divAbout;

            currentPage = 0;
            currentPageActive =  0;

            try {
                sectionWithVideos = body.querySelector('.video-section');
                sectionLeft = body.querySelector( '.video-section-left');
                sectionRight = body.querySelector('.video-section-right');
                while( sectionWithVideos.firstChild ) {
                    sectionWithVideos.removeChild( sectionWithVideos.firstChild );
                }
                while( sectionLeft.firstChild ) {
                    sectionLeft.removeChild( sectionLeft.firstChild );
                }
                while( sectionRight.firstChild ) {
                    sectionRight.removeChild( sectionRight.firstChild );
                }
            }
            catch( error ) {
                swipeSection = document.createElement('section');
                sectionLeft = document.createElement('section');
                sectionWithVideos = document.createElement('section');
                sectionRight = document.createElement('section');

                sectionWithVideos.className = 'video-section';
                sectionLeft.className = 'video-section-left';
                sectionRight.className = 'video-section-right';
                swipeSection.className = 'swipe-section';

                swipeSection.appendChild( sectionLeft);
                swipeSection.appendChild(sectionWithVideos);
                swipeSection.appendChild(sectionRight);

                swipeSection.addEventListener('click', clickVideos);
                body.appendChild(swipeSection);
            }

            if( document.querySelector( '.panel-paging') )
                body.removeChild( document.querySelector( '.panel-paging') );

            for(var i = 0; i < numbVideoOnDisplay; i++) {
                sectionWithVideos.appendChild( createItemVideo( resultRequest[i], i) );
                if( i + numbVideoOnDisplay < arrayWithVideos.length )
                sectionRight.appendChild( createItemVideo( resultRequest[i + numbVideoOnDisplay ], i + numbVideoOnDisplay));
            }

            var divPaging = document.createElement('div'),
            ul = document.createElement('ul'), li;

            divPaging.className = 'panel-paging';
            ul.className = 'paging';

            for( i = 0; i < 5; i++ ) {
                li = document.createElement('li');
                if( i === 0)
                    li.className = 'list-paging-active';
                else
                    li.className = 'list-paging';

                li.appendChild( document.createTextNode( i + 1 ) );
                li.setAttribute('id', 'id_' + i );
                ul.appendChild(li);
            }

            divPaging.appendChild(ul);
            body.appendChild(divPaging);

            delete callbackRegistry[callbackName];
            body.removeChild( document.querySelector("[data-request='"+callbackName+"']") );

            successRequest = true;

            if( !currentSection)
                currentSection = body.querySelector('.video-section');
            if( ! body.onmouseup )
                initEventsSwipe();
        };

        function checkCallback() {
            if (successRequest) {  checkForLoadVideos(); }
            else {
                body.removeChild( document.querySelector("[data-request='"+callbackName+"']") );
                delete callbackRegistry[callbackName];
            }
        }

        script.onreadystatechange = function() {

            if (this.readyState == 'complete' || this.readyState == 'loaded'){
                this.onreadystatechange = null;
                setTimeout(checkCallback, 0);
            }
        }

        script.onload = checkCallback;
        script.src = src;
        body.appendChild(script);
    }

    body.onkeyup = function (e) {
        e  = e || event;
        if (e.keyCode == 13) {
           sendRequest();
        }
    }

    window.onunload = function () {
        window.localStorage.clear();

        var request = inputSearch.value;
        if( request.length > 0)
            window.localStorage.setItem('request', request);
    };

    window.onresize = function() {
        var screenWidth = window.innerWidth,
            oldNumbVideoOnDisplay = numbVideoOnDisplay,
            i, indexLeft, indexRight, currentIndex, itemLi, activePage;

        if (screenWidth > 945)
            numbVideoOnDisplay = 3;
        else if (screenWidth <= 945 && screenWidth > 600)
            numbVideoOnDisplay = 2;
        else
            numbVideoOnDisplay = 1;

        if( currentSection ) {

            if( oldNumbVideoOnDisplay !== numbVideoOnDisplay) {
                while (currentSection.firstChild) {
                    currentSection.removeChild(currentSection.firstChild);
                }
                while (sectionLeft.firstChild) {
                    sectionLeft.removeChild(sectionLeft.firstChild);
                }
                while (sectionRight.firstChild) {
                    sectionRight.removeChild(sectionRight.firstChild);
                }

                checkForLoadVideos();

                currentIndex = currentPage * oldNumbVideoOnDisplay;

                if( numbVideoOnDisplay === 3)
                    while( currentIndex % 3 !== 0)
                        currentIndex--;

                if( numbVideoOnDisplay === 2)
                    while( currentIndex % 2 !== 0)
                        currentIndex--;

                for (i = 0; i < numbVideoOnDisplay; i++) {

                    indexLeft = currentIndex - numbVideoOnDisplay;
                    indexRight = currentIndex + numbVideoOnDisplay;
                    currentSection.appendChild( createItemVideo( arrayWithVideos[currentIndex], currentIndex ) );
                    if( indexLeft >=0 )
                        sectionLeft.appendChild( createItemVideo( arrayWithVideos[indexLeft], indexLeft ) );
                    if ( indexRight < arrayWithVideos.length )
                        sectionRight.appendChild( createItemVideo( arrayWithVideos[indexRight], indexRight ) );

                    currentIndex++;
                }

                currentPage = Math.floor(currentPage * oldNumbVideoOnDisplay / numbVideoOnDisplay);

                if( currentPage < 2)
                    activePage = currentPage;
                else if( (currentPage + 3) * numbVideoOnDisplay >= arrayWithVideos.length )
                    activePage = 4;
                else if( (currentPage + 2) * numbVideoOnDisplay >= arrayWithVideos.length )
                    activePage = 3;
                else
                    activePage = 2;

                for( i = 0 ; i < 5; i++) {
                    itemLi = body.querySelector('#id_'+i );
                    if( i === activePage )
                        itemLi.className = 'list-paging-active';
                    else
                        itemLi.className = 'list-paging';
                    itemLi.firstChild.data = currentPage + i + 1 - activePage;
                }
            }
        }
    }

    function touchHandler(event)
    {
        var touches = event.changedTouches,
            first = touches[0],
            type = "";
        switch(event.type)
        {
            case "touchstart": type = "mousedown"; break;
            case "touchmove":  type="mousemove"; break;
            case "touchend":   type="mouseup"; break;
            default: return;
        }

        var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent(type, true, true, window, 1,
            first.screenX, first.screenY,
            first.clientX, first.clientY, false,
            false, false, false, 0, null);

        first.target.dispatchEvent(simulatedEvent);
        if( event.type === 'touchstart' || event.type === 'touchend') {}
        else
            event.preventDefault();
    }

    function initEventsSwipe() {

        body.addEventListener("touchstart", touchHandler, true);
        body.addEventListener("touchmove", touchHandler, true);
        body.addEventListener("touchend", touchHandler, true);
        body.addEventListener("touchcancel", touchHandler, true);

        body.addEventListener('mouseup', mouseUp);
        body.addEventListener('mousemove', mouseMove);
        body.addEventListener('mousedown', mouseDown);
        document.querySelector('.paging').addEventListener( 'click', pagingClick );
    }

    function renderingNewPagingAndSections() {

        var activePage, itemLi, i,
            currentIndex, indexLeft, indexRight;

        if( currentPage < 2)
            activePage = currentPage;
        else if( (currentPage + 3) * numbVideoOnDisplay >= arrayWithVideos.length )
            activePage = 4;
        else if( (currentPage + 2) * numbVideoOnDisplay >= arrayWithVideos.length )
            activePage = 3;
        else
            activePage = 2;

        while (currentSection.firstChild) {
            currentSection.removeChild(currentSection.firstChild);
        }
        while (sectionLeft.firstChild) {
            sectionLeft.removeChild(sectionLeft.firstChild);
        }
        while (sectionRight.firstChild) {
            sectionRight.removeChild(sectionRight.firstChild);
        }

        currentIndex = currentPage * numbVideoOnDisplay;
        for (i = 0; i < numbVideoOnDisplay; i++) {
            indexLeft = currentIndex - numbVideoOnDisplay;
            indexRight = currentIndex + numbVideoOnDisplay;
            currentSection.appendChild( createItemVideo( arrayWithVideos[currentIndex], currentIndex ) );
            if( indexLeft >=0 )
                sectionLeft.appendChild( createItemVideo( arrayWithVideos[indexLeft], indexLeft ) );
            if ( indexRight < arrayWithVideos.length )
                sectionRight.appendChild( createItemVideo( arrayWithVideos[indexRight], indexRight ) );

            currentIndex++;
        }

        for( i = 0 ; i < 5; i++) {
            itemLi = body.querySelector('#id_'+i );
            if( i === activePage )
                itemLi.className = 'list-paging-active';
            else
                itemLi.className = 'list-paging';
            itemLi.firstChild.data = currentPage + i + 1 - activePage;
        }
    }

    function pagingClick( event ) {

        var newPage, ul;
        if( event.target.tagName === 'LI') {
            if( event.target.classList.contains('list-paging') ) {
                ul = body.querySelector('.paging');
                newPage = event.target.firstChild.data - 0;

                currentPage = newPage - 1;

                checkForLoadVideos();

                renderingNewPagingAndSections();
            }
        }

        event.preventDefault()
    }

    function clickVideos( event ) {
        if( oldX !== event.pageX || oldY !== event.pageY) {
            event.preventDefault();
        }
    }

    function checkForLoadVideos() {
           if( arrayWithVideos.length  <= ( (6 + currentPage) * numbVideoOnDisplay ) ) {
               var callbackName = 'f'+String(Math.random()).slice(2);
               successRequest = false;
               var src, script;

               src = 'http://gdata.youtube.com/feeds/api/videos/?' +
                       'callback=callbackRegistry.' + callbackName +
                       '&v=2&alt=json&' +
                       'max-results=15&start-index=' + arrayWithVideos.length +
                       '&q='+ request;

               script = document.createElement('script');
               script.src = src;
               script.setAttribute('data-request', callbackName);
               body.appendChild(script);

               callbackRegistry[callbackName] = function(data) {

                   arrayWithVideos = arrayWithVideos.concat( data.feed.entry );

                   delete callbackRegistry[callbackName];
                   body.removeChild( document.querySelector("[data-request='"+callbackName+"']") );

                   successRequest = true;
               };

               function checkCallback() {
                   if (!successRequest) {
                       body.removeChild( document.querySelector("[data-request='"+callbackName+"']") );
                       delete callbackRegistry[callbackName];
                   }
               }

               script.onreadystatechange = function() {

                   if (this.readyState == 'complete' || this.readyState == 'loaded'){
                       this.onreadystatechange = null;
                       setTimeout(checkCallback, 0);
                   }
               }

               script.onload = checkCallback;
               script.src = src;
               body.appendChild(script);

           }
    }

    function reloadPaging( direction ) {

        var panelPaging = document.querySelector('.paging'), itemLi, i,
            id = panelPaging.querySelector('.list-paging-active').id[3] - 0;
        if( direction === 'right') {

            if( id < 2) {
                if( ( id + 1) * numbVideoOnDisplay < arrayWithVideos.length) {
                    panelPaging.querySelector('.list-paging-active').className = 'list-paging';
                    itemLi = panelPaging.querySelector( '#id_'+ (id + 1) );
                    itemLi.className = 'list-paging-active';
                }
            }
            else if( id === 2) {
                if( (currentPage + 2) * numbVideoOnDisplay > arrayWithVideos.length) {
                    panelPaging.querySelector('.list-paging-active').className = 'list-paging';
                    itemLi = panelPaging.querySelector( '#id_'+ (id + 1) );
                    itemLi.className = 'list-paging-active';
                }
                else {
                    for( var i = 0 ; i < 5; i++) {
                        itemLi = panelPaging.querySelector('#id_'+i );
                        itemLi.firstChild.data = itemLi.firstChild.data - 0 + 1;

                    }
                }
            }
        }
        else {
            if(  id  > 2 ) {
                panelPaging.querySelector('.list-paging-active').className = 'list-paging';
                itemLi = panelPaging.querySelector( '#id_'+ (id - 1) );
                itemLi.className = 'list-paging-active';
            }
            else if ( id <= 2 ) {
                if( currentPage + 1 > 2 ) {
                    for( var i = 0 ; i < 5; i++) {
                        itemLi = panelPaging.querySelector('#id_'+i );
                        itemLi.firstChild.data = itemLi.firstChild.data - 1;

                    }
                }
                else  {
                        panelPaging.querySelector('.list-paging-active').className = 'list-paging';
                        itemLi = panelPaging.querySelector('#id_' + (id - 1));
                        itemLi.className = 'list-paging-active';

                }
            }
        }
    }

    function swipeRight( ) {

        currentSection.removeEventListener('transitionend', swipeRight);

        checkForLoadVideos();
        reloadPaging( 'right' );
        sectionLeft.remove();
        sectionLeft = currentSection;
        sectionLeft.className = 'video-section-left';
        currentSection = sectionRight;
        currentSection.className = 'video-section';
        sectionRight = document.createElement('section');
        sectionRight.className = 'video-section-right';
        for( var i = 0; i < numbVideoOnDisplay; i++)
            if( (currentPage + 1)*numbVideoOnDisplay < arrayWithVideos.length)
            sectionRight.appendChild(
                createItemVideo( arrayWithVideos[ (currentPage + 1)*numbVideoOnDisplay  + i], (currentPage+1)*numbVideoOnDisplay + i ) );
        swipeSection.appendChild( sectionRight );
    };

    function swipeLeft( ) {

        currentSection.removeEventListener('transitionend', swipeLeft);

        reloadPaging( 'left' );
        sectionRight.remove();
        sectionRight = currentSection;
        sectionRight.className = 'video-section-right';
        currentSection = sectionLeft;
        currentSection.className = 'video-section';
        sectionLeft = document.createElement('section');
        sectionLeft.className = 'video-section-left';
        for( var i = 0; i < numbVideoOnDisplay; i++) {
            if ((currentPage - 1) * numbVideoOnDisplay >= 0)
                sectionLeft.appendChild(
                    createItemVideo(arrayWithVideos[ (currentPage - 1) * numbVideoOnDisplay + i], (currentPage - 1) * numbVideoOnDisplay + i));
        }
        swipeSection.appendChild( sectionLeft );
    };

    function doSwipe( direction ) {
        if( direction === 'center') {
            sectionLeft.style.transform = 'translate3d( 0, 0, 0)';
            currentSection.style.transform = 'translate3d( 0, 0, 0)';
            sectionRight.style.transform = 'translate3d( 0, 0, 0)';

            sectionLeft.removeAttribute('style');
            currentSection.removeAttribute('style');
            sectionRight.removeAttribute('style');
        }
        else if ( direction === 'left') {
            currentPage++;
            currentSection.addEventListener('transitionend', swipeRight);
            sectionLeft.className += ' swipe-page-left';
            currentSection.className += ' swipe-page-left';
            sectionRight.className += ' swipe-page-left';

            sectionLeft.removeAttribute('style');
            currentSection.removeAttribute('style');
            sectionRight.removeAttribute('style');

        }
        else {
            currentPage--;
            currentSection.addEventListener('transitionend', swipeLeft);
            sectionLeft.className += ' swipe-page-right';
            currentSection.className += ' swipe-page-right';
            sectionRight.className += ' swipe-page-right';

            sectionLeft.removeAttribute('style');
            currentSection.removeAttribute('style');
            sectionRight.removeAttribute('style');
        }
    }

    function mouseDown( event ) {
        mouseClick = true;
        oldX = event.pageX;
        oldY = event.pageY;
        delta = 0;
    };

    function mouseUp( event ) {

        if( event.pageX !== oldX || event.pageY === oldY && mouseClick) {

            sectionLeft.className += ' swipe-page-animating';
            currentSection.className += ' swipe-page-animating';
            sectionRight.className += ' swipe-page-animating';
            var direction = 'center';

            if( delta > 200 ) {
                direction = 'left';
                if( !sectionRight.firstChild)
                    direction = 'center';
            }
            else if ( delta < -200) {
                direction = 'right';
                if( !sectionLeft.firstChild)
                    direction = 'center';
            }

            mouseClick = false;
            doSwipe( direction );
        }
    };

    function mouseMove( event ) {
        if( mouseClick ) {
            delta = oldX - event.pageX;

            if( delta > 300)
                delta = 300;
            else if ( delta < -300)
                delta = -300;

            sectionLeft.style.transform = 'translate3d( '+(-delta)+'px, 0, 0)';
            currentSection.style.transform = 'translate3d( '+(-delta)+'px, 0, 0)';
            sectionRight.style.transform = 'translate3d( '+(-delta)+'px, 0, 0)';
            event.stopPropagation();
            event.preventDefault();

        }
    };

 })();

